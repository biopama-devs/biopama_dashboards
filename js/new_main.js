/* MAIN SCRIPOT TO LOAD CARDS APPLICATIONS */

jQuery(document).ready(function($) {

  /*
  function checkSize(){
    $( '.biopama-chart' ).each(function() {
      $( this ).resize();
    });
  }

  $( window ).resize(function() {
    checkSize();
  });
  */


  var first_load = true;
  var bkgd_imgs = [ 
    '/modules/custom/biopama_dashboards/images/bkgd_pa_dashboard.jpg',
  ];

  setTimeout(resetHeaderBkgd(), 500);

    function resetHeaderBkgd(){
      var idx = Math.floor(Math.round(Math.random() * (bkgd_imgs.length - 1),1));

      if (first_load){
        $('#page').css("background-image", 'url("'+bkgd_imgs[idx]+'")');
        setTimeout(function(){ $('#page').fadeIn(4000);},200);
      }
    }


  // When the user clicks on the button, scroll to the top of the document
  function topFunction() {
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
      resetHeaderBkgd();
  }

  $('#myBtn_scrollTop').on('click',function(){topFunction();});

  //background CARDS
  $('.hidden_background').each(function(){
    var img_url = $(this).html();
    if (img_url != ""){
      $(this).parent().css('background-image','url('+img_url+')');
      $(this).parent().css('background-size','cover');
    }
  });
});
