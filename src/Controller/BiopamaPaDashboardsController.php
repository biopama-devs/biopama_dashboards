<?php
/**
 * @file
 * Contains \Drupal\biopama_dashboards\Controller\BiopamaPaDashboardsController.
 */
 
namespace Drupal\biopama_dashboards\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines Protected Area Dashboard class.
 */
class BiopamaPaDashboardsController extends ControllerBase {
	
  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function content() {
    $element = array(
        '#theme' => 'page_pa_dashboard',
      );
      return $element;
  }
  
}